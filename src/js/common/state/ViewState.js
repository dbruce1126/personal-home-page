'use strict';

import EventEmitter from 'events';

class ViewState extends EventEmitter
{
  static CHANGE_EVENT = 'event';
  constructor(window) {
    super();
    this.window = window;
    this.viewState = {
      'contactModalOpen': false,
    };
  }
  getState() {
    return this.viewState;
  }
  updateContactModalOpen(value) {
    this.viewState.contactModalOpen = value;
    this.emit(ViewState.CHANGE_EVENT);
  }
  toggleContactModal() {
    this.updateContactModalOpen(!this.viewState.contactModalOpen);
  }
  showContactModal() {
    this.updateContactModalOpen(true);
  }
  hideContactModal() {
    this.updateContactModalOpen(false);
  }
}

if (typeof window !== 'object') {
  window = {

  };
}
export default new ViewState(window);
