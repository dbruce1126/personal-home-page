'use strict';

import React from 'react';
import { Link } from 'react-router';
import { Navbar, Nav, CollapsibleNav, NavItem } from 'react-bootstrap';

import ViewState from '../state/ViewState';

export default class Navigation extends React.Component
{
  constructor(props) {
    super(props);
    this.showContactModal = this.showContactModal.bind(this);
  }
  showContactModal(event) {
    event.preventDefault();
    ViewState.showContactModal();
  }
  render() {
    return (
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to={ '/' }>
              <i className="fa fa-home fa-lg fa-fw" /> Home
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <li eventKey={1} role="presentation">
              <Link to={ '/tech' }>
                <i className="fa fa-code fa-lg fa-fw hidden-sm" /> Technologies
              </Link>
            </li>
            <li eventKey={2} role="presentation">
              <Link to={ '/oss' }>
                <i className="fa fa-linux fa-lg fa-fw hidden-sm" /> Open Source
              </Link>
            </li>
            <li eventKey={3} role="presentation">
              <Link to={ '/employment' }>
                <i className="fa fa-briefcase fa-lg fa-fw hidden-sm" /> Employment
              </Link>
            </li>
            <li eventKey={4} role="presentation">
              <a href="#" onClick={ this.showContactModal }>
                <i className="fa fa-envelope fa-lg fa-fw hidden-sm" /> Contact Me
              </a>
            </li>
          </Nav>
          <hr className="visible-xs" />
          <Nav pullRight>
            <NavItem eventKey={1} href="https://github.com/danbruce" target="_blank">
              <i className="fa fa-github fa-lg fa-fw" />
              <span className="hidden-lg hidden-md hidden-sm">Github</span>
            </NavItem>
            <NavItem eventKey={2} href="https://linkedin.com/pub/daniel-bruce/29/418/79" target="_blank">
              <i className="fa fa-linkedin-square fa-lg fa-fw" />
              <span className="hidden-lg hidden-md hidden-sm">LinkedIn</span>
            </NavItem>
            <NavItem eventKey={3} href="https://twitter.com/dbruce84" target="_blank">
              <i className="fa fa-twitter fa-lg fa-fw" />
              <span className="hidden-lg hidden-md hidden-sm">Twitter</span>
            </NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}