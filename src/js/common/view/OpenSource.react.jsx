'use strict';

var React = require('react');

/** react bootstrap components */
var ReactBootstrap = require('react-bootstrap');
var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;
var PageHeader = ReactBootstrap.PageHeader;

var OpenSource = React.createClass({
  render: function() {
    return (
      <Grid>
        <Row>
          <PageHeader>Open Source</PageHeader>
          <Col xs={12}>
            <h1>DUnit</h1>
          </Col>
        </Row>
      </Grid>
    );
  }
});

module.exports = OpenSource;