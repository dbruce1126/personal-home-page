'use strict';

var React = require('react');

/** react bootstrap components */
var ReactBootstrap = require('react-bootstrap');
var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;
var PageHeader = ReactBootstrap.PageHeader;

var Tech = React.createClass({
  render: function() {
    return (
      <Grid>
        <PageHeader>
          Technologies &amp; Skills
        </PageHeader>
        <Row>
          <Col lg={6} md={12}>
            <h3>Programming Languages</h3>
            <ul className="list-unstyled list-inline">
              <li>PHP,</li>
              <li>Javascript,</li>
              <li>Objective-C (iOS),</li>
              <li>Java,</li>
              <li>R,</li>
              <li>Python</li>
            </ul>
          </Col>
          <Col lg={6} md={12}>
            <h3>Markup and Presentation Formats</h3>
              <ul className="list-unstyled list-inline">
                <li>HTML,</li>
                <li>CSS,</li>
                <li>JSON,</li>
                <li>XML,</li>
                <li>LaTeX</li>
              </ul>
          </Col>
        </Row>
        <Row>
          <Col lg={6} md={12}>
            <h3>PHP Frameworks</h3>
            <ul className="list-unstyled list-inline">
              <li>Zend Framework 1 &amp; 2,</li>
              <li>Phalcon PHP,</li>
              <li>CakePHP,</li>
              <li>WordPress</li>
            </ul>
          </Col>
          <Col lg={6} md={12}>
            <h3>Frontend Frameworks</h3>
            <ul className="list-unstyled list-inline">
              <li>JQuery,</li>
              <li>Bootstrap,</li>
              <li>AngularJs,</li>
              <li>Highcharts,</li>
              <li>Mustache.js</li>
            </ul>
          </Col>
        </Row>
        <Row>
          <Col lg={6} md={12}>
            <h3>Web Stack Technologies</h3>
            <ul className="list-unstyled list-inline">
              <li>Linux (Debian, Ubuntu, Gentoo),</li>
              <li>Apache,</li>
              <li>MySQL,</li>
              <li>Node.js,</li>
              <li>Nginx,</li>
              <li>Memcache,</li>
              <li>Gearman</li>
            </ul>
          </Col>
          <Col lg={6} md={12}>
            <h3>Other Technology</h3>
            <ul className="list-unstyled list-inline">
              <li>Git,</li>
              <li>XCode,</li>
              <li>PHPUnit,</li>
              <li>Vim,</li>
              <li>Unix shell</li>
            </ul>
          </Col>
        </Row>
      </Grid>
    );
  }
});

module.exports = Tech;