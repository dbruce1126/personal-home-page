'use strict';

var React = require('react');

var EmploymentMediaItem = React.createClass({
  propTypes: {
    item: React.PropTypes.object.isRequired
  },
  render: function() {
    var logoUri = '/images/' + this.props.item.logoUri;
    var logo = (
      <a href={ this.props.item.uri }>
        <img className="media-object img-thumbnail" src={ logoUri } alt={ this.props.item.companyName } width="128" height="128" />
      </a>
    );
    var heading = (
      <h4 className="media-heading">
       { this.props.item.positionTitle } at <a href={ this.props.item.uri }>{ this.props.item.companyName }</a><br className="visible-xs" />
       <small> ( { this.props.item.startDate } &mdash; { this.props.item.endDate } )</small>
      </h4>
    );
    return (
      <div className="media">
        <div className="media-left">
          { logo }
        </div>
        <div className="media-body">
          { heading }
          { this.props.item.details }
        </div>
      </div>
    );
  }
});

module.exports = EmploymentMediaItem;