'use strict';

import React from 'react';
import { Modal, ListGroup, ListGroupItem, Button } from 'react-bootstrap';

import ViewState from '../state/ViewState';

/*
* Created by http://liljosh.com
* Modified by Dan Bruce
* Formats a phone number to be in +1 (555) 555-5555 format
*/
function formatPhoneNumber (phone) {
  phone = phone.replace(/[^0-9]/g, '');
  phone = phone.replace(/(\d{1})(\d{3})(\d{3})(\d{4})/, "\+$1 ($2) $3-$4");
  return phone;
};

const EMAIL_ADDRESS = 'dbruce1126@gmail.com';
const PHONE_NUMBER = '16132557042';

export default class ContactModal extends React.Component {
  static propTypes = {
    open: React.PropTypes.bool.isRequired
  };
  constructor(props) {
    super(props);
    this.hideModal = this.hideModal.bind(this);
  }
  hideModal() {
    ViewState.hideContactModal();
  }
  render() {
    return (
      <Modal show={ this.props.open } onHide={ this.hideModal }>
        <Modal.Header closeButton>
          <Modal.Title>Contact Me</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ListGroup>
            <ListGroupItem header="Email">
              <a href={ 'mailto:' + EMAIL_ADDRESS }>
                <i className="fa fa-envelope fa-lg" /> { EMAIL_ADDRESS }
              </a>
            </ListGroupItem>
            <ListGroupItem header="Phone">
              <a href={ 'tel:' + PHONE_NUMBER }>
                <i className="fa fa-phone fa-lg" /> { formatPhoneNumber(PHONE_NUMBER) }
              </a>
            </ListGroupItem>
            <ListGroupItem header="On Twitter">
              <a href="https://twitter.com/home/?status=@danbruce" target="_blank">
                <i className="fa fa-twitter fa-lg" /> Tweet to me
              </a>
            </ListGroupItem>
            <ListGroupItem header="On LinkedIn">
              <a href="https://linkedin.com/pub/daniel-bruce/29/418/79" target="_blank">
                <i className="fa fa-linkedin fa-lg" /> Message me on LinkedIn
              </a>
            </ListGroupItem>
          </ListGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={ this.hideModal }>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

