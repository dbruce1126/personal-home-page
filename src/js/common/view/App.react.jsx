'use strict';

import React from 'react';
import ViewState from '../state/ViewState';

import Navigation from './Navigation.react.jsx';
import ContactModal from './ContactModal.react.jsx';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.updateState = this.updateState.bind(this);
  }
  componentDidMount() {
    ViewState.on(ViewState.CHANGE_EVENT, this.updateState);
  }
  componentWillUnmount() {
    ViewState.removeListener(ViewState.CHANGE_EVENT, this.updateState);
  }
  updateState() {
    console.log('updating');
    this.forceUpdate();
  }
  render() {
    const viewState = ViewState.getState();
    return (
      <main>
        <Navigation {...viewState} />
        { this.props.children }
        <ContactModal
          open={ viewState.contactModalOpen } />
      </main>
    );
  }
}