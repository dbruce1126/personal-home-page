'use strict';

var React = require('react');

/** react bootstrap components */
var ReactBootstrap = require('react-bootstrap');
var Grid = ReactBootstrap.Grid;
var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;
var PageHeader = ReactBootstrap.PageHeader;

var EmploymentMediaItem = require('./EmploymentMediaItem.react.jsx');

var _fitchek = {
  companyName: 'Fitchek',
  logoUri: 'fitchek.png',
  uri: '//fitchek.com/',
  positionTitle: 'Technical Lead',
  startDate: 'July 2015',
  endDate: 'Present',
  details: [
    (<p key={1}>
      I am currently leading engineering efforts at Fitchek. I am responsible
      for building all aspects of the web software stack and leading the team to
      deliver our SAS solution for fitness professionals.
    </p>)
  ]
};

var _vectorface = {
  companyName: 'Vectorface',
  logoUri: 'vectorface.png',
  uri: '//www.vectorface.com/',
  positionTitle: 'Backend Developer',
  startDate: 'March 2014',
  endDate: 'June 2015',
  details: [
    (<p key={1}>
      I worked with VectorFace&rsquo;s online casino software performing various
      backend tasks such as feature development, bug fixes, and code
      maintenance. In particular, my background in mathematics allowed me to
      employ statistically sound methods for verification of the games of chance
      leading to more efficient and accurate estimation of game payouts.
    </p>)
  ]
};

var _mediamiser = {
  companyName: 'MediaMiser Ltd.',
  logoUri: 'mediamiser.png',
  uri: '//www.mediamiser.com/',
  positionTitle: 'Team Lead & Lead Mobile Developer',
  startDate: 'February 2011',
  endDate: 'October 2013',
  details: [
    (<p key={1}>
      I was hired in the position of Lead Mobile Developer and lead the
      company&rsquo;s efforts in building their iOS applications. I was
      eventually promoted to a Team Lead position and led a team of developers
      in building the web frontend to MediaMiser&rsquo;s next generation SAS
      solution,
      <a href="http://www.mediamiser.com/video-tour/" target="_blank">MediaMiser SNAP</a>.
    </p>)
  ]
};

var _carleton = {
  companyName: 'Carleton University',
  logoUri: 'carleton.png',
  uri: '//carleton.ca/',
  positionTitle: 'Bachelor of Mathematics',
  startDate: '2003',
  endDate: '2009',
  details: [
    (<p key={1}>
      I graduated with high honours in 2009 and was on the Dean&rsquo;s Honour
      List in 2008 and 2009. My program focused on computer science and
      statistics.
    </p>),
    (<p key={2}>
      <a href="/files/honours_project.pdf">Final year honours project (PDF)</a>
    </p>)
  ]
};

var Employment = React.createClass({
  render: function() {
    return (
      <Grid>
        <PageHeader>
          Employment History &amp; Education
          <small style={{display: 'none'}}> &mdash;<a href="/files/resume.pdf" download>resume.pdf</a></small>
        </PageHeader>
        <Row>
          <Col lg={12}>
            <EmploymentMediaItem item={ _fitchek } />
            <hr />
            <EmploymentMediaItem item={ _vectorface } />
            <hr />
            <EmploymentMediaItem item={ _mediamiser } />
            <hr />
            <h3>Education</h3>
            <EmploymentMediaItem item={ _carleton } />
          </Col>
        </Row>
      </Grid>
    );
  }
});

module.exports = Employment;