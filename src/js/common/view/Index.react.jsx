'use strict';

var React = require('react');

/** react bootstrap components */
var ReactBootstrap = require('react-bootstrap');
var Grid = ReactBootstrap.Grid;
var Row = ReactBootstrap.Row;
var Col = ReactBootstrap.Col;
var PageHeader = ReactBootstrap.PageHeader;

var Index = React.createClass({
  render: function() {
    return (
      <Grid>
        <PageHeader>
          Dan Bruce <small>Full Stack Web Application Developer</small>
        </PageHeader>
        <Row>
          <Col md={3} mdOffset={1} sm={5} smOffset={1} xs={8} xsOffset={2}>
            <img src="/images/headshot.jpg" className="img-circle headshot" alt="Daniel Bruce Headshot" />
          </Col>
          <Col md={6} mdOffset={2} sm={5} smOffset={1} xs={12}>
            <h1>
              Technical Lead at <a href="http://fitchek.com" target="_blank">Fitchek</a>
            </h1>
            <p className="lead">
              At Fitchek, I build a SAS application providing business
              management tools to organizations and individuals working in the
              fitness industry. <a href="/#work">Read more</a>.
            </p>
          </Col>
        </Row>
      </Grid>
    );
  }
});

module.exports = Index;