'use strict';

import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import App from './view/App.react.jsx';
import Index from './view/Index.react.jsx';
import Tech from './view/Tech.react.jsx';
import OpenSource from './view/OpenSource.react.jsx';
import Employment from './view/Employment.react.jsx';

// declare our routes and their hierarchy
export default (
  <Router history={ browserHistory }>
    <Route path="/" component={App}>
      <IndexRoute component={Index} />
      <Route path="tech" component={Tech} />
      <Route path="oss" component={OpenSource} />
      <Route path="employment" component={Employment} />
    </Route>
  </Router>
);