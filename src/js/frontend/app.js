'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import Routes from '../common/Routes.react.jsx';

ReactDOM.render(Routes, document.getElementById('content'));